export interface IUser {
  id: number
  name: string
  location: string
  occupation: string
}