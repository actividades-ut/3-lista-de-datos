import type { IUser } from "@/interfaces/IUser"

const USERS_DATA: IUser[] = [
  { id: 1, name: 'Juan Pérez', location: 'Ciudad de México', occupation: 'Ingeniero de Software' },
  { id: 2, name: 'Ana García', location: 'Buenos Aires', occupation: 'Diseñadora Gráfica' },
  { id: 3, name: 'Carlos Ramirez', location: 'Madrid', occupation: 'Analista de Datos' },
  { id: 4, name: 'Laura Martínez', location: 'Lima', occupation: 'Periodista' },
  { id: 5, name: 'Diego Torres', location: 'Santiago', occupation: 'Ingeniero Civil' },
  { id: 6, name: 'Isabel López', location: 'Bogotá', occupation: 'Psicóloga Clínica' },
  { id: 7, name: 'Miguel Rodríguez', location: 'Caracas', occupation: 'Arquitecto' },
  { id: 8, name: 'Carmen Sánchez', location: 'Quito', occupation: 'Profesora de Historia' },
  { id: 9, name: 'Gonzalo Fernández', location: 'Montevideo', occupation: 'Médico' },
  { id: 10, name: 'María Herrera', location: 'Lisboa', occupation: 'Empresaria' },
  { id: 11, name: 'Ricardo Silva', location: 'Panamá', occupation: 'Abogado' }
]

export default USERS_DATA
