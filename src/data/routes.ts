import IconHomeVue from "@/components/icons/IconHome.vue"
import IconProjectVue from "@/components/icons/IconProject.vue"
import IconGalleryVue from "@/components/icons/IconGallery.vue"
import IconBellVue from "@/components/icons/IconBell.vue"
import IconMessageVue from "@/components/icons/IconMessage.vue"
import IconSettingsVue from "@/components/icons/IconSettings.vue"

const ROUTES = [
  {
    path: "/",
    name: "Inicio",
    icon: IconHomeVue
  },
  {
    path: "/projects",
    name: "Proyectos",
    icon: IconProjectVue
  },
  {
    path: "/gallery",
    name: "Galería",
    icon: IconGalleryVue
  },
  {
    path: "/notifications",
    name: "Notificaciones",
    icon: IconBellVue
  },
  {
    path: "/messages",
    name: "Mensajes",
    icon: IconMessageVue
  },
  {
    path: "/settings",
    name: "Configuraciones",
    icon: IconSettingsVue
  },
]

export default ROUTES